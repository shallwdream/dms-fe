import {Component, OnInit} from '@angular/core';
import {Router, NavigationEnd, RouteConfigLoadStart, NavigationError} from '@angular/router';
import {NzMessageService} from 'ng-zorro-antd';
import {ScrollService, MenuService, SettingsService} from '@delon/theme';

@Component({
    selector: 'layout-default',
    templateUrl: './default.component.html'
})
export class LayoutDefaultComponent implements OnInit {


    isFetching = false;

    constructor( public router: Router,
                scroll: ScrollService,
                private _message: NzMessageService,
                public menuSrv: MenuService,
                public settings: SettingsService) {
        // scroll to top in change page
        router.events.subscribe(evt => {
            if (!this.isFetching && evt instanceof RouteConfigLoadStart) {
                this.isFetching = true;
            }
            if (evt instanceof NavigationError) {
                this.isFetching = false;
                _message.error(`无法加载${evt.url}路由`, {nzDuration: 1000 * 3});
                return;
            }
            if (!(evt instanceof NavigationEnd)) {
                return;
            }
            setTimeout(() => {
                scroll.scrollToTop();
                this.isFetching = false;
            }, 100);
        });
    }


    ngOnInit(): void {
        // if (this.isEmpty(this.settings.user)) {
        //     this.router.navigateByUrl('/passport/login')
        // }
    }

    /*
     * 检测对象是否是空对象(不包含任何可读属性)。
     * 方法既检测对象本身的属性，也检测从原型继承的属性(因此没有使hasOwnProperty)。
     */
    isEmpty(obj) {
        for (var name in obj) {
            return false;
        }
        return true;
    };
}
