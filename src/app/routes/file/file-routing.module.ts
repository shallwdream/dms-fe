import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {FileManageComponent} from "./file-manage/file-manage.component";
import {FileUploadComponent} from "./file-upload/file-upload.component";

const routes: Routes = [
    {path: 'file-manage', component: FileManageComponent},
    {path: 'file-upload', component: FileUploadComponent}
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class FileRoutingModule {
}
