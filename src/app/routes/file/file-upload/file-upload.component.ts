import { Component, OnInit } from '@angular/core';
import { _HttpClient } from '@delon/theme';

import {filter} from 'rxjs/operators/filter';
// tslint:disable
import {HttpRequest, HttpClient, HttpResponse} from '@angular/common/http';
import {NzMessageService, UploadFile} from 'ng-zorro-antd';

@Component({
  selector: 'app-file-upload',
  templateUrl: './file-upload.component.html',
})
export class FileUploadComponent implements OnInit {

    ngOnInit() {
    }

    uploading = false;
    fileList: UploadFile[] = [];

    constructor(private http: HttpClient, private msg: NzMessageService) {
    }

    beforeUpload = (file: UploadFile): boolean => {
        this.fileList.push(file);
        return false;
    }

    handleUpload() {
        const formData = new FormData();
        this.fileList.forEach((file: any) => {
            formData.append('files[]', file);
        });
        this.uploading = true;
        // You can use any AJAX library you like
        const req = new HttpRequest('POST', 'https://jsonplaceholder.typicode.com/posts/', formData, {
            // reportProgress: true
        });
        this.http.request(req).pipe(filter(e => e instanceof HttpResponse)).subscribe((event: any) => {
            this.uploading = false;
            this.msg.success('upload successfully.');
        }, (err) => {
            this.uploading = false;
            this.msg.error('upload failed.');
        });
    }

}
