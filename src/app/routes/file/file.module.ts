import { NgModule } from '@angular/core';
import { SharedModule } from '@shared/shared.module';
import { FileRoutingModule } from './file-routing.module';
import { FileManageComponent } from './file-manage/file-manage.component';
import { FileUploadComponent } from './file-upload/file-upload.component';

const COMPONENT_NOROUNT = [];

@NgModule({
  imports: [
    SharedModule,
    FileRoutingModule
  ],
  declarations: [
      ...COMPONENT_NOROUNT,
      FileManageComponent,
      FileUploadComponent
  ],
  entryComponents: COMPONENT_NOROUNT
})
export class FileModule { }
