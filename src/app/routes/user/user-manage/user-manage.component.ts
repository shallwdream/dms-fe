import {Component, OnInit} from '@angular/core';
import {_HttpClient} from '@delon/theme';

@Component({
    selector: 'app-user-manage',
    templateUrl: './user-manage.component.html',
})
export class UserManageComponent implements OnInit {

    constructor(private http: _HttpClient) {
    }

    ngOnInit() {
    }

}
