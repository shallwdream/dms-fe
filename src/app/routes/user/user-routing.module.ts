import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {UserManageComponent} from "./user-manage/user-manage.component";

const routes: Routes = [
    {path: 'manage', component: UserManageComponent}
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class UserRoutingModule {
}
