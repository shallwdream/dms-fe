import { NgModule } from '@angular/core';
import { SharedModule } from '@shared/shared.module';
import { UserRoutingModule } from './user-routing.module';
import { UserManageComponent } from './user-manage/user-manage.component';

const COMPONENT_NOROUNT = [];

@NgModule({
  imports: [
    SharedModule,
    UserRoutingModule
  ],
  declarations: [
      ...COMPONENT_NOROUNT,
      UserManageComponent
  ],
  entryComponents: COMPONENT_NOROUNT
})
export class UserModule { }
